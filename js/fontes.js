$(function(){

	$("#germinal").click(function(){
		$(this).addClass("styleon"),
		$("#floreal").removeClass("styleon"),
		$("#fructidor").removeClass("styleon"),
		$("body").addClass("germinal");
		$("body").removeClass("floreal");
		$("body").removeClass("fructidor");
	});

	$("#floreal").click(function(){
		$(this).toggleClass("styleon"),
		$("#germinal").removeClass("styleon"),
		$("#fructidor").removeClass("styleon"),
		$("body").addClass("floreal");
		$("body").removeClass("germinal");
		$("body").removeClass("fructidor");
	});

	$("#fructidor").click(function(){
		$(this).addClass("styleon"),
		$("#floreal").removeClass("styleon"),
		$("#germinal").removeClass("styleon"),
		$("body").addClass("fructidor");
		$("body").removeClass("germinal");
		$("body").removeClass("floreal");
	});

	$("#regular").click(function(){
		$(this).addClass("styleon"),
		$("#bold").removeClass("styleon"),
		$("#italic").removeClass("styleon"),
		$("body").addClass("regular");
		$("body").removeClass("bold");
		$("body").removeClass("italic");
	});

	$("#bold").click(function(){
		$(this).addClass("styleon"),
		$("#regular").removeClass("styleon"),
		$("body").addClass("bold");
		$("body").removeClass("regular");
	});

	$("#italic").click(function(){
		$(this).addClass("styleon"),
		$("#regular").removeClass("styleon"),
		$("body").addClass("italic");
		$("body").removeClass("regular");
	});
});
