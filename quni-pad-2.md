## Pratiques en commun de normes molles, rageuses
					
Les fontes de la [typothèque](http://genderfluid.space/typotheque) proposent de nombreux glyphes (lettres mutantes) en addition au point médian et autres solutions régulièrement utilisées pour écrire et composer des textes inclusifs. Nos claviers ne contiennent pas (encore) les touches qui correspondent à ces caractères et ces ligatures. Alors pour rendre utilisable cet arc-en-ciel de signes, nous sommes en train de construire un ensemble de tours de main, des pratiques en commun, de normes molles, rageuses et aux petits oignons, qui ensemble forment læ présente Queer Unicode Initiative (QUNI).

![Exemple en Baskervvol](https://safe.genderfluid.space/apps/files_sharing/publicpreview/FRa6HMa7yaMnb8e?x=1318&y=316&a=true&file=2021_QUNI_baskervvol_img3.png&scalingup=0)


## Le standard Unicode comme terrain vague

Cette grande prairie de jeux et d'expériences inclusives est aménagée à l'intérieur de la Supplementary Private Use Area-A (PUA-A) (https://en.wikipedia.org/wiki/Private_Use_Areas#PUA-A) du très industriel standard Unicode, centré sur l'alphabet latin (https://fr.wikipedia.org/wiki/Unicode). On n'a pas le choix, c'est le "leading standard" qui est utilisé par tous les téléphones, ordinateurs et logiciels qui nous entourent. Pour y injecter le QUNI qu'il mérite, on campe dans un bloc terrain vague en bordure, et c'est évidement la zone la plus cool. Nous nous sommes inspirés du [Medieval Unicode Font Initiative (MUFI)](https://mufi.info/), un projet visant à coordonner l’encodage et l’affichage de caractères médiévaux écrits en alphabet latin.

### Tableau ici

## Pour qui? Pour plein de nous!

Læ QUNI est développæ :

* Pour que les utilisatrix des fontes puissent accéder et utiliser les glyphes inclusifves dans leurs textes d'une manière compatible entre différentes fontes;

![Vue sur un même bout de texte composés avec deux fontes qunifiées](https://safe.genderfluid.space/apps/files_sharing/publicpreview/Kji2zbcr2AHp8KK?x=1920&y=623&a=true&file=2021_QUNI_baskervvol_BNM_adelphe_img1.png&scalingup=0)

* Pour que les dessinatrix des fontes puissent poser les glyphes dans les cases unicodes communes;

![Cases Unicode dans Glyphs](https://safe.genderfluid.space/apps/files_sharing/publicpreview/Hri6WHCnkaxrS5w?x=1318&y=259&a=true&file=2021_QUNI_liste-glyphs.png&scalingup=0)

* Pour que les mêmes dessinatrix des fontes puissent utiliser les fonctionalités opentype (features) déjà codées, ou ajouter les leurs selon une logique partagée.

![Morceau de code de features Opentype](https://safe.genderfluid.space/apps/files_sharing/publicpreview/sLeKkgymzEoxaay?x=1920&y=623&a=true&file=2021_QUNI_features_img1.png&scalingup=0)


## Au boulot!

Qunifier (ajouter les glyphes inclusifs selon le QUNI) une fonte est aisé, on a identifié 58 formes de glyphes qui couvre la plupart des besoins, que nous avons pour l'instant nommé « base ». Il suffit de les dessiner et de les ajouter à votre fonte selon l'encodage du QUNI (voir onglet « Pour les utilisatrix »). Voir notre screencast pour qunifier votre fonte avec Glyphs ou avec FontForge!

〈 Ici bientôt screenshots des deux screencasts 〉


## Quatre modes de fonctionnement

Nous avons répertoriés plusieurs modes de fonctionnement des fontes inclusives existantes.

### Ligatures
Il y a deux possibilités de ligatures au moins. Les ligatures de base s'opèrent entre deux caractères séparés par un point médian. Les ligatures plus fondues permettent des agglomérations de caractères. Exemple : c·he pour blanc/blanche. Les formes fondues permettent aussi d'agglémérer les formes au pluriel.

![Exemple de quelques glyphes ligaturés dans la fonte Baskervvol](https://safe.genderfluid.space/s/9YzBxNqmfXiwwsT/preview)

### Formes alternatives
Les caractères alternatifs isolés non-binaires sont conçus pour être utilisés hors des usages d'accord pour permettre un usage débinarisé de chaque caractère. Ils permetent aussi l'usage de lettrines spécifiques.

![Exemple de quelques glyphes alternatifs  ə dans la fonte Combine] IMAGE à REMPLACER (https://safe.genderfluid.space/s/Lz7qBmbjCJ6zDa9/preview)

### Signes diacritiques
Les signes diacritiques font partie de systèmes d'accentuation qui permettent de marquer les terminaisons genrées, comme des bornes indiquant le passage d'un genre à un autre.

![Exemple de quelques glyphes avec diacritiques dans la fonte Adelphe] IMAGE à REMPLACER (https://safe.genderfluid.space/s/TFGEySPzpdg9zjn/preview)

### Acadam
L'acadam avec ses suffixes non-binaires permet de traduire un texte écrit en point médian en suffixes non genrés.
![Exemple d'usage de l'Acadam dans la fonte Deliæ](https://safe.genderfluid.space/s/98p4pxB45xmGaDS/preview)


## Le QUNI gonflable

Le QUNI est prêt à de très nombreux ajouts, c'est une initiative blobesque, et en novembre 2021, ce sont 154 signes ou groupes de glyphes inclusifs qui sont susceptibles d'être farcis de dessins de lettres. Et de plus en plus de fontes se font garnir de ces lettres additionnelles en mode QUNI. Chacun de ces glyphes s'est vu attribuer une valeur Unicode de la PUA-A et peut être enrichi de quinze variantes de dessin, et ça dans chaque fonte. L'initiative a des perspectives d'agrandissement, entre autres pour d'autres langues et systèmes d'écriture, puisqu'en tout ce sont 3496 cases qui sont disponibles pour y injecter de nouvelles propositions de glyphes, et 52440 variantes!

![Le tableau de la PUA](https://safe.genderfluid.space/apps/files_sharing/publicpreview/BtneAXHEbMD4FKg?x=1318&y=316&a=true&file=2021_QUNI_PUA.png&scalingup=0)

![Le système proposé pour les alternatives](https://safe.genderfluid.space/s/Sekgae58K9Sy4oX/preview)


## Pour les dessinatrix

Pour **dessiner** des glyphes inclusifs dans des fontes, le QUNI est construit autour de trois éléments-outils :
* **Quoi** : *la liste des glyphes* proposés à dessiner.
* **Où** les mettre : *le tableau des point de codage* Unicode qui y correspondent.
* **Comment** les faire apparaitre (y accéder) : *les fonctionnalités opentype* avec lesquelles équiper les fontes.
Tout ces outils sont repris dans le tableau du QUNI.

### Quoi - la liste des glyphes
La liste des glyphes reprend le max de glyphes qui ont été injectés par les typographes des premières fontes de la typothèque. Ce sont très largement des glyphes utilisés dans le français. C'est une liste très ouverte, elle va certainement être enrichie pour encore environ dix millénaires (au moins l'âge du patriarcat).

### Où mettre ces glyphes
Le tableau des point de codage permet de savoir dans quelle case mettre quel glyphe. Le fait d'avoir choisi de profiter de la PUA permet d'avoir à la fois des noms de glyphes (exemple: eacute_e), utilisés entre autres dans les features OpenType, mais aussi des codes unicode (U+....) utilisables dans les logiciels rustiques ou, dans certains cas, sur le web. En gros, cela permet d'être le plus largement compatible.

### Comment accéder à ces glyphes
Les features OpenType permettent d'appeler les glyphes inclusifs grâce à un système de remplacements automatiques. Ainsi il suffit à l'utilisatrix de la fonte de rédiger en inclusif en utilisant le point médian pour appeler le glyphe adéquat. Par exemple é·e est automatiquement remplacé par le glyphe "eacute_e".


## Les fichiers

Tous les fichiers de référence sont versionnés sur notre dépot [gitlab.com/bye-bye-binary/quni](https://gitlab.com/bye-bye-binary/quni) et téléchargeables ci-dessous.

* [Télécharger le fichier des fonctionnalités Opentype .fea]([gitlab.com/bye-bye-binary/quni/xxxxxx)
* [Télécharger le tableau du QUNI en format spreadsheet]([gitlab.com/bye-bye-binary/quni/xxxxxx)
* [Télécharger le tableau du QUNI extensif dans sa PUA complète ]([gitlab.com/bye-bye-binary/quni/xxxxxx)



# Pourquoi le QUNI 

	> Il n'est plus nécessaire ici d'insister sur l'importance de l'écriture inclusive, hein. Si on en parle souvent en termes d'orthographe, on en parle souvent moins en typographie. Pourtant l'écriture inclusive utilise des lettres et des signes. Que ce soit dans une langue très genrée comme le français, ou beaucoup moins comme l'anglais.
	
	#### Des ligatures amies
	
	> En typographie, les ligatures désignent des combinaisons de deux ou plusieurs caractères fusionnés pour des raisons esthétiques (ff, fi, ffl, …) ou linguistiques (æ, œ). Parce qu’elles sont fondées sur le lien et les transitions plus que sur la séparation, les ligatures sont un terrain de travail plein de promesses pour l’écriture inclusive. Créer de nouvelles associations de lettres (par exemple pour lier les terminaisons en « -if » et en « -ives » comme dans inclusifve) nécessite de créer de nouvelles cases dans la liste des caractères d’une fonte. Définie par le consortium international et industriel Unicode, cette liste prévoit pour chaque caractère (techniquement appelé glyphe) un code permettant notamment le passage d’une fonte à une autre en maintenant la correspondance entre caractères. Cette liste est régulièrement mise à jour par Unicode, et donc susceptible d’accueillir de nouveaux caractères — un enjeu à venir pour la typographie non-binaire.
		
	#### Une méthode commune
	
	> Différentes fontes proposent déjà des glyphes inclusifs. La typothèque BBB*** a entrepris de les lister et de la présenter. La famille de fontes Baskervvol propose un set de glyphes inclusifs créés par BBB en collective. Le nombre de ces glyphes est suffisament important pour s'intéresser à leurs emplacements, noms, codages Unicode, accessibilité, et modes d'utilisation. Pour faciliter les travaux en cours et futurs, il nous semble important de partager une méthode commune pour la création de fontes inclusives. Nous avons nommé la méthode sur laquelle nous travaillons la Queer Unicode Initiative ou mieux, QUNI. Un peu à la manière du Medieval Unicode Font Initiative (MUFI), projet visant à coordonner l’encodage et l’affichage de caractères médiévaux écrits en alphabet latin. Le codage Unicode de QUNI a été placé dans la Supplementary Private Use Area-A*** (une plage libre, utilisable par tout le monde), offrant la palette la plus large possible d'accès, y compris sans utiliser les fonctionnalités OpenType***, sur le web par exemple. QUNI a l’ambition de coordonner collectivement le travail des dessinateur·ices de fontes qui intègrent des glyphes inclusifs pour faciliter leur production, accroître leur accessibilité, notamment dans les logiciels de traitement de texte grand public, et mettre en évidence l'importance de la présence grandissante de ces glyphes dans de plus en plus de fontes.
	
	#### "Vous vous prenez un peu la tête, non?"
	
	> Dans https://www.arllfb.be/actualite/ecriture_inclusive.pdf des détracteurs de l'écriture inclusive avancent que
	
	> "L’Académie rappelle les deux caractères fondamentaux du signe linguistique : l’arbitraire et la linéarité. Les conséquences de cet arbitraire sont que la langue ne représente pas directement le réel et ne détermine pas la pensée, dans la mesure où les locuteurs d’une même langue peuvent exprimer des conceptions très différentes. Quant à la linéarité, certaines pratiques de l’écriture inclusive, dont le point médian et les « néologismes morphologiques » créés par amalgames (celleux), exigent un décryptage empêchant une lecture linéaire essentielle à la compréhension d’un texte (exemple : tou·te·s ou tou·t·es sénateur·rice·s, usant de segments inexistants). L’Académie se prononce clairement contre ces formes contre-intuitives et très instables."
	
	> Nous pensons que cet arbitraire peut aussi être développé dans la typographie, et que les signes peuvent être dessinés par "amalgames morphologiques" pour agir sur la fluidité, d'une manière volontairement positive, ou volontairement négative et ça a été le propre de la typographie, et du lettrage, de l'écriture, et cela même avant l'invention des caractères mobiles. De plus, l'impact du langage sur la pensée est complètement admis en linguistique et en psychologie (hypothèse Sapir-Whorf), et les recherches récentes en morphologie linguistique tendent à montrer que les signes linguistiques sont motivés (ou en tout cas pas complètement arbitraires).
	
	> * [Arbitraire ou motivation des structures linguistiques? Le tournant de 1980](https://journals.openedition.org/edl/2376#bodyftn5)
	> * [Les motivations du signe linguistique : vers une typologie des relations signifiant/signifié](http://cle.ens-lyon.fr/plurilangues/langue/miscellanees/les-motivations-du-signe-linguistique-vers-une-typologie-des-relations-signifiant-signifie)
	> * [La motivation du nom face à l'arbitraire du signe : à propos de Shoah](https://www.persee.fr/doc/hispa_0007-4640_2005_num_107_1_5232)
	
	#### Des fontes rendues inclusives
	
	> Le Baskervvol (2018–2021) est un projet évolutif et collectif, il est dans les mains de huit dessinateur·ices au sein de Bye Bye Binary à l'été 2021 et ce nombre ne cesse d'augmenter. Le fil des utilisations fait sans cesse remonter des besoins. À ce jour, ce caractère est augmenté de jeux stylistiques OpenType permettant l’activation de ligatures conditionnelles. Des versions variables sont en préparation. D'autres efforts de typographies inclusives en cours, comme l'Adelphe et la VG5000, amènent leur expérience variée pour tenter d'élargir le champ d'application de QUNI. À l'avenir, dans l'idéal, QUNI devrait intégrer le plus possible de solutions différentes pour répondre aux besoins très larges des enjeux complexes de la typographie inclusive.
		
	#### La crête de l'écriture inclusive en français, du code, de la typographie et du langage
	
	> QUNI est une proposition ouverte, pensée pour évoluer et s'enrichir. Elle marche sur la crête accidentée de l'écriture inclusive en français, du code, de la typographie et du langage. QUNI doit intégrer des éléments parfois discordants, et en tout cas très variés. N'hésitez pas à nous signaler tout problème, toute partie moins claire, toute incompréhension! Bienvenue!
	
	*Attention : Le texte ci-joint détaille son fonctionnement pour les typographes et les personnes qui dessinent des caractères. Pour toutes les personnes qui écrivent et mettent en page, un mode d'emploi simple est disponible sur [typotheque.genderfluid.space/utilisation](http://typotheque.genderfluid.space/utilisation).*